import { TestBed } from '@angular/core/testing';

import { GtLibService } from './gt-lib.service';

describe('GtLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GtLibService = TestBed.get(GtLibService);
    expect(service).toBeTruthy();
  });
});
