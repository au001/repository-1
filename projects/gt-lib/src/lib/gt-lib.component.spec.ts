import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GtLibComponent } from './gt-lib.component';

describe('GtLibComponent', () => {
  let component: GtLibComponent;
  let fixture: ComponentFixture<GtLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GtLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GtLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
