import {Component, Input, OnInit} from '@angular/core';
import {GtDataModel} from "../models/gt-data-model";

@Component({
  selector: 'gt-page',
  templateUrl: './gt-page.component.html',
  styleUrls: ['./gt-page.component.less']
})
export class GtPageComponent implements OnInit {

  @Input()
  gtDataModel: GtDataModel;

  ngOnInit(): void {
    console.log(this.gtDataModel);
    /**
     * @todo add pipe and mapper so
     */
    this.gtDataModel.gtBaseService.getList().subscribe(
      (next) => {
        console.log(next);
      },
      (error) => {
        console.log(error);
      }
    )
  }
}
