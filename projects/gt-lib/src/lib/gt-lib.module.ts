import { NgModule } from '@angular/core';
import { GtLibComponent } from './gt-lib.component';
import {GtPageComponent} from "./page/gt-page.component";
import {CommonModule} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";



@NgModule({
  declarations: [GtLibComponent, GtPageComponent],
  imports: [
    CommonModule
  ],
  providers: [HttpClientModule],
  exports: [GtLibComponent, GtPageComponent]
})
export class GtLibModule { }
