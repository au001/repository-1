import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export abstract class GtBaseService {

  protected constructor(httpClient: HttpClient) { }

  public abstract getList(): Observable<object>;
}
