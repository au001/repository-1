import { Component, OnInit } from '@angular/core';
import {GtDataModel} from "./models/gt-data-model";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'gt-lib',
  template: `
      <gt-page *ngIf="gtDataModel" [gtDataModel]="gtDataModel"></gt-page>
  `,
  styles: []
})
export class GtLibComponent implements OnInit {

  /**
   * Data model
   */
  private _gtDataModel: GtDataModel;

  /**
   * Parameter
   */
  private _id: number;
  /**
   * @var Component
   */
  private eventHandler;
  /**
   * @var method
   */
  private eventAction;

  /**
   *
   * @param route
   */
  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(
      params => {
        this._id = params.id;
      }
    )
  }

  ngOnInit() {
  }

  /**
   * Populate data and sends to eventHandler parameters
   *
   * @param self GtLibComponent
   * @param eventHandler Component
   * @param eventAction method
   */
  populate(self: GtLibComponent, eventHandler, eventAction) {
    self.eventHandler = eventHandler;
    self.eventAction = eventAction;

    self.eventAction(self.eventHandler, self.id, self)
  }

  get gtDataModel(): GtDataModel {
    return this._gtDataModel;
  }

  set gtDataModel(value: GtDataModel) {
    this._gtDataModel = value;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }
}
