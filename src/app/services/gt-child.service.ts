import { Injectable } from '@angular/core';
import {GtBaseService} from "../../../projects/gt-lib/src/lib/services/gt-base.service";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {GtMapper} from "../../../projects/gt-lib/src/lib/helpers/mappers/gt-mapper";
import {GtDataHttpModel} from "../../../projects/gt-lib/src/lib/models/http-models/gt-data-http-model";

@Injectable({
  providedIn: 'root'
})
export class GtChildService implements GtBaseService {

  /**
   *
   * @param httpClient
   */
  constructor(private httpClient: HttpClient) {
  }


  /**
   *
   */
  public getList(): Observable<GtDataHttpModel> {
    return this.httpClient.get('/assets/data.json').pipe(
      map(response => {
        return GtMapper.map(response)
      })
    );
  }

}
