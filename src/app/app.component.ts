import { Component } from '@angular/core';
import {GtLibComponent} from "../../projects/gt-lib/src/lib/gt-lib.component";
import {GtDataModel} from "../../projects/gt-lib/src/lib/models/gt-data-model";
import {GtChildService} from "./services/gt-child.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'workspace';

  id = null;
  childReference: GtLibComponent;

  constructor(private gtChildService: GtChildService) {

  }

  // region events

  /**
   * When router outlet loads or changes, this event is launch with a component instance as parameter
   *
   * @param component
   */
  onRouterOutletActivate(component) {
    if(component instanceof GtLibComponent) {
      /**
       * When component instance is a GtLibComponent, we call it asking for launch an event onGtLibComponentIdPopulate
       *  so it sends us its url parameters, such ids and so on
       */
      component.populate(component, this, this.onGtLibComponentIdPopulate);
    }
  }

  /**
   * Event action after get the parameters from the child component
   *
   * @param self
   * @param id
   * @param childReference
   */
  onGtLibComponentIdPopulate(self: AppComponent, id, childReference: GtLibComponent) {
    /**
     * Set id on component
     */
    self.id = id;
    /**
     * Set child reference on component
     */
    self.childReference = childReference;

    /**
     * Populate GtLibComponent
     */
    self.populateGtLibComponent();
  }

  // endregion events

  // region actions



  // endregion actions

  // region populate

  /**
   * Populate needed model for GtLibComponent
   */
  populateGtLibComponent() {
    /**
     * Instantiate GtDataModel for Gt Library
     */
    const gtDataModel = new GtDataModel();

    /**
     * Populate GtDataModel with children service
     */
    gtDataModel.gtBaseService = this.gtChildService;

    /**
     * Send to childReference GtDataModel
     */
    this.childReference.gtDataModel = gtDataModel;
  }

  // endregion populate
}
