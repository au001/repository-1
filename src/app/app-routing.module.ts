import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GtLibComponent} from "../../projects/gt-lib/src/lib/gt-lib.component";


const routes: Routes = [
  {
    path: '',
    component: GtLibComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
